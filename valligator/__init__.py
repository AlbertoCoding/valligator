"""Patch validation."""
from . import simple_validators

VALIDATORS = {
    'contains-keyval': simple_validators.contains_keyval,
    'subject-contains-keyword': simple_validators.subject_contains_keyword,
    'must-have-cover': simple_validators.must_have_cover,
    'max-line-length': simple_validators.max_line_length,
    'depends': simple_validators.generic_patch_dependency,
    'all-files': simple_validators.all_files
}
