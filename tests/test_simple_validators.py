"""Tests for valligator.simple_validators."""
import re
import unittest

from valligator import simple_validators


class TestContainsKeyval(unittest.TestCase):
    """Tests for simple_validators.contains_keyval()."""
    def test_no_keyval(self):
        """Verify contains_keyval fails if the key is not present."""
        patch = 'This is a sample text in the patch'
        self.assertFalse(
            simple_validators.contains_keyval('', [patch], 'key')
        )

    def test_keyval_missing_patch(self):
        """
        Verify contains_keyval fails if the key is not present in all patches.
        """
        patch_true = 'text\nkey: value'
        patch_false = 'This is a sample text in the patch'
        self.assertFalse(simple_validators.contains_keyval(
            '', [patch_true, patch_false], 'key'
        ))

    def test_keyval_in_cover(self):
        """Verify contains_keyval passes if the key is in the cover."""
        cover = 'text\nkey: value'
        patch = 'This is a sample text in the patch'
        self.assertTrue(
            simple_validators.contains_keyval(cover, [patch], 'key')
        )

    def test_keyval_in_patches(self):
        """
        Verify contains_keyval passes if the key is present in all patches.
        """
        patches = ['text\nkey: value', 'something\n\nkey: value2\n\n']
        self.assertTrue(
            simple_validators.contains_keyval('', patches, 'key')
        )


class TestSubjectContainsKeyword(unittest.TestCase):
    """Tests for simple_validators.contains_keyval()."""

    def test_no_cover_no_keyword_in_patch(self):
        """
        Verify subject_contains_keyword fails if given only patches,
        keyword is not in the patch subject
        """
        patch = 'This is a sample text in the patch'
        self.assertFalse(
            simple_validators.subject_contains_keyword('', [patch], 'key')
        )

    def test_no_cover_no_keyword_in_one_patch(self):
        """
        Verify subject_contains_keyword fails if the keyword
        is not present in only one of the patches.
        """
        patch_true = 'text\nSubject: [ key ]: value'
        patch_false = 'This is a sample text in the patch'
        self.assertFalse(simple_validators.subject_contains_keyword(
            '', [patch_true, patch_false], 'key'
        ))

    def test_keyword_in_cover_and_patches(self):
        """
        Verify subject_contains_keyword passes if the keyword
        is in the cover and in the patches
        """
        cover = 'Subject: [ key: value]'
        patch = 'Subject: [This is a sample text in the patch with the key ]'
        self.assertTrue(
            simple_validators.subject_contains_keyword(cover, [patch], 'key')
        )

    def test_no_cover_keyword_in_all_patches_subject(self):
        """
        Verify subject_contains_keyword passes if the keyword
        is present in all patches given no cover.
        """
        patches = ['Subject: [ text key ]',
                   'Subject: [be key 1/21 qsdf/as]']
        self.assertTrue(
            simple_validators.subject_contains_keyword('', patches, 'key')
        )

    def test_no_keyword_one_patch(self):
        """
        Verify subject_contains_keyword fails if the keyword
        is not present in only one of the patches given a cover
        and multiple patches.
        """
        cover = 'Subject: [ key: value]'
        patch_true = 'text\nSubject: [ key ]: value'
        patch_false = 'This is a sample text in the patch'
        self.assertFalse(simple_validators.subject_contains_keyword(
            cover, [patch_true, patch_false], 'key'
        ))


class TestMustHaveCover(unittest.TestCase):
    """Tests for existence of Cover Letter when it is required."""

    def test_required_and_no_cover(self):
        """
        Verify must_have_cover fails when a cover is required
        and it is not included.
        """

        cover = ''
        must = 'trUE'
        patches = ['patch1 content', 'patch2 content']
        self.assertFalse(simple_validators.must_have_cover(
            cover, patches, must
        ))

    def test_required_and_cover(self):
        """
        Verify must_have_cover passes when a cover is required
        and it is included.
        """

        cover = 'cover content'
        must = 'TrUe'
        patches = ['patch1 content', 'patch2 content']
        self.assertTrue(simple_validators.must_have_cover(
            cover, patches, must
        ))

    def test_not_required_and_no_cover(self):
        """
        Verify must_have_cover passes when a cover is not required
        and it is not included.
        """

        cover = ''
        must = 'false'
        patches = ['patch1 content', 'patch2 content']
        self.assertTrue(simple_validators.must_have_cover(
            cover, patches, must
        ))

    def test_not_required_and_cover(self):
        """
        Verify must_have_cover passes when a cover is not required
        and it is included.
        """

        cover = 'cover content'
        must = 'False'
        patches = ['patch1 content', 'patch2 content']
        self.assertTrue(simple_validators.must_have_cover(
            cover, patches, must
        ))

    def test_bad_word(self):
        """
        Verify must_have_cover fails and alerts when an
        unrecognized argument is provided (ignores the check)
        and returns immediately.
        """

        cover = 'cover content'
        must = 'qwerty'
        patches = ['patch1 content', 'patch2 content']
        self.assertFalse(simple_validators.must_have_cover(
            cover, patches, must
        ))

    def test_series_more_patches_no_cover(self):
        """
        Verify must_have_cover fails when series is activated,
        more than one patch is provided and there is no cover.
        """

        cover = ''
        must = 'series'
        patches = ['patch1 content', 'patch2 content']
        self.assertFalse(simple_validators.must_have_cover(
            cover, patches, must
        ))

    def test_series_more_patches_cover(self):
        """
        Verify must_have_cover passes when series is activated,
        more than one patch is provided and there is a cover.
        """

        cover = 'cover content'
        must = 'series'
        patches = ['patch1 content', 'patch2 content']
        self.assertTrue(simple_validators.must_have_cover(
            cover, patches, must
        ))

    def test_series_less_patches_no_cover(self):
        """
        Verify must_have_cover passes when series is activated
        and one or less patches are provided even though there
        is no cover.
        """

        cover = ''
        must = 'series'
        patches = ['patch2 content']
        self.assertTrue(simple_validators.must_have_cover(
            cover, patches, must
        ))


class TestMaxLineLength(unittest.TestCase):
    """Tests to check the maximum length of the patches lines"""

    def test_empty_patch(self):
        """
        Verify max_line_length passes when a provided patch is empty.
        """

        cover = '+asdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdf'
        max_len = '40'
        patches = ['']
        self.assertTrue(simple_validators.max_line_length(
            cover, patches, max_len
        ))

    def test_patch_with_long_line(self):
        """
        Verify max_line_length fails when a provided patch has a line
        longer than the maximum length allowed.
        """

        cover = ''
        max_len = '20'
        patches = ['+Alberto - this is a loooooongggg sentence over the limit']
        self.assertFalse(simple_validators.max_line_length(
            cover, patches, max_len
        ))

    def test_patch_with_short_line(self):
        """
        Verify max_line_lenght passes when a provided patch has a line
        shorter than the maximum length allowed.
        """

        cover = ''
        max_len = '20'
        patches = ['+AlbertoRRI']
        self.assertTrue(simple_validators.max_line_length(
            cover, patches, max_len
        ))


class TestGenericPatchDependency(unittest.TestCase):
    """Tests to check whether there is any patch dependency"""

    def test_any_dependency(self):
        """
        Verify generic_patch_dependency fails when there is any
        patch dependency.
        """

        cover = ''
        key = 'Blocked by'
        patches = ['Depends on: TelecoLabs/patchesAlberto', 'patch2 content']
        self.assertFalse(simple_validators.generic_patch_dependency(
            cover, patches, key
        ))

    def test_no_dependency(self):
        """
        Verify generic_patch_dependency passes when there is no
        patch dependency.
        """

        cover = ''
        key = 'Blocked by'
        patches = ['patch1 content', 'patch2 content']
        self.assertTrue(simple_validators.generic_patch_dependency(
            cover, patches, key
        ))

    def test_non_default_dependency(self):
        """
        Verify generic_patch_dependency fails when there is a
        non-default patch dependency indicated by the argument.
        """

        cover = ''
        key = 'Blocked by'
        patches = ['patch1 content\nBlocked by: TelecoLabs',
                   'patch2 content']
        self.assertFalse(simple_validators.generic_patch_dependency(
            cover, patches, key
        ))


class TestAllFiles(unittest.TestCase):
    """Tests for existence of all the patches in a whole patches serie"""

    def test_all_files(self):
        """
        Verify all_files passes when all the patches of a patches
        serie are provided.
        """

        cover = ''
        key = 'true'
        patches = ['Subject: asdf [1/2] \npatch1 content',
                   'Default content\nSubject: patch2 content [2/2]']
        self.assertTrue(simple_validators.all_files(
            cover, patches, key
        ))

    def test_not_all_files(self):
        """
        Verify all_files fails when not all the patches of a patches
        serie are provided.
        """
        cover = ''
        key = 'TrUe'
        patches = ['Subject: asdf [1/2] \npatch1 content']
        self.assertFalse(simple_validators.all_files(
            cover, patches, key
        ))

    def test_all_files_with_false_argument(self):
        """
        Verify all_files passes when the argument is false
        independently of the patches provided.
        """
        cover = ''
        key = 'False'
        patches = ['Subject: asdf [1/2] \npatch1 content',
                   'Default content\nSubject: patch2 content [2/2]']
        self.assertTrue(simple_validators.all_files(
            cover, patches, key
        ))

    def test_no_file(self):
        """
        Verify all_files fails when no patch is provided.
        """
        cover = ''
        key = 'True'
        patches = []
        self.assertFalse(simple_validators.all_files(
            cover, patches, key
        ))

    def test_single_patch_no_cover(self):
        """
        Verify all_files passes when a single patch is provided
        without any cover.
        """
        cover = ''
        key = 'trUE'
        patches = ['Subject: asdf \npatch1 content']
        self.assertTrue(simple_validators.all_files(
            cover, patches, key
        ))

    def test_wrong_argument(self):
        """
        Verify all_files fails when a wrong argument is provided.
        """
        cover = ''
        key = 'AlbertoRRI'
        patches = ['Subject: asdf [1/2] \npatch1 content',
                   'Default content\nSubject: patch2 content [2/2]']
        self.assertFalse(simple_validators.all_files(
            cover, patches, key
        ))
